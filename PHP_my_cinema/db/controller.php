<?php
if (isset($_GET['mot']) && stripos($_GET['mot'], ':g') !== FALSE){
  $mot = htmlspecialchars($_GET['mot']);
  $mot = substr($mot, 3);
  $query = "SELECT *
    FROM film
    INNER JOIN genre ON film.id_genre = genre.id_genre
    WHERE nom
    LIKE '%".$mot."%' ";
  $films = $db->query($query);
  $to_pag = $films;
  include 'view/pagination.php';
  $films = $to_pag;
}

else if (isset($_GET['mot']) && stripos($_GET['mot'], ':m') !== FALSE){
  $mot = htmlspecialchars($_GET['mot']);
  $mot = substr($mot, 3);
  $query = "SELECT *
    FROM  fiche_personne
    WHERE  nom
    LIKE  '".$mot."%'
    OR  prenom
    LIKE  '".$mot."%'";
  $membres = $db->query($query);
  $to_pag = $membres;
  include 'view/pagination.php';
  $membres = $to_pag;
}

else if (isset($_GET['mot']) && stripos($_GET['mot'], ':d') !== FALSE){
  $mot = htmlspecialchars($_GET['mot']);
  $mot = substr($mot, 3);
  $query = "SELECT *
    FROM film
    INNER JOIN distrib ON film.id_distrib = distrib.id_distrib
    WHERE  nom
    LIKE  '%".$mot."%'";
  $distribs = $db->query($query);
  $to_pag = $distribs;
  include 'view/pagination.php';
  $distribs = $to_pag;
}

else if (isset($_GET['mot']) !== FALSE){
  $mot = htmlspecialchars($_GET['mot']);
  $query = "SELECT *
    FROM film
    INNER JOIN genre ON film.id_genre = genre.id_genre
    WHERE titre
    LIKE '%".$mot."%' ";
  $films = $db->query($query);
  $to_pag = $films;
  include 'view/pagination.php';
  $films = $to_pag;
}
?>
