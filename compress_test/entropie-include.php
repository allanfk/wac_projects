<?php

/**
 * @param string $t source dont les symboles sont à compter
 * @return array(array[string]integer tableau associatif symbole => occurences,
 *               integer nombre d'occurences totales) tableau 
 *
 * inconvénients de cette implémentation:
 *    - calcul de strlen($t) *et* de $total
 *    - mise en mémoire du tampon entier sous forme de tableau,
 *      ce qui gaspille de la mémoire
 * -> une meilleure implémentation serait possible si le programme
 *    principal nous appelait ligne par ligne pour compléter
 *    un tableau d'occurence, par exemple! ou si l'on consommait
 *    dans la fonction occurences() une entrée fichier, par exemple.
 */

function occurences($t) {  
   return array(array_count_values(str_split($t)), strlen($t));//, strlen($t)));
}

   /* variante plus manuelle */
function occurences_more_manual($t) {
   $a = array();

   # définition d'une fonction anonyme qui a besoin d'utiliser
   # une référence au tableau local $a à la fonction occurences
   # pour pouvoir le modifier
   $callback = function ($val, $index) use (&$a) {
      if (!array_key_exists($val, $a)) {
         $a[$val] = 0;
      }
      $a[$val]++;
   };

   # appliquer la fonction anonyme à l'ensemble du tableau
   if (strlen($t)) {
      array_walk(str_split($t), $callback);
   }
   return array($a, strlen($t));
}

/**
 * @param array[string]integer $o tableau associatif symbole => occurences
 * @param integer $t nombre total d'occurences
 * @return float valeur de l'entropie sur $o
 *
 * alternative: array_map() et array_sum() pour éviter la boucle manuelle
 */
function calculer_entropie($o, $t) {
   $H = 0;
   foreach ($o as $symbol => $count) { /* ou sans le $symbol => */
      $Pi = $count/$t;
      $Hi = -log($Pi)/log(2); /* ou voir 2e argument de la fonction log() */
      $H += $Pi * $Hi;
   }

   return $H;
}

?>