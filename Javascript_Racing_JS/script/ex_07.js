window.onload = function() {
// Init
  var canvas = document.getElementsByTagName('canvas')[0];
  var button = document.getElementsByTagName('button');
  var music = new Audio('http://red-mp3.su/stream/3131883/2-be-3-partir-un-jour.mp3');
// Canvas
  var ctx = canvas.getContext('2d');
		 ctx.beginPath();
		 ctx.strokeStyle = "white";
		 ctx.moveTo(6, 6);
		 ctx.lineTo(14, 10);
		 ctx.lineTo(6, 14);
		 ctx.closePath();
		 ctx.stroke();

// Player
  canvas.onclick = function() {
    music.play();
  }
  button[0].onclick = function() {
    music.pause();
  }
  button[1].onclick = function() {
    music.pause();
    music.currentTime = 0;
  }
  button[2].onclick = function() {
    music.muted = (music.muted == true) ? false : true;
  }
};
