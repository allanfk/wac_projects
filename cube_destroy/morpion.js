///// init Canvas
var square = document.createElement("canvas"); // Créer le canvas
document.body.appendChild(square); // Ajouts du canvas au body
square.setAttribute("id", "myCanvas"); // Ajouts  de l'id au canvas
 // Ajout du style myCanvas
document.getElementById("myCanvas").setAttribute(
 "style", "height: 320px; background-color: #eee; box-shadow: 0px 0px 10px #00000080; display:block; top:50%; position: absolute; left:50%; transform: translate(-50%, -50%);");

///// La base du Canavas
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var ballRadius = 10;
var x = canvas.width/2;
var y = canvas.height-30;
var dx = 2;
var dy = -2;

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();

    if(x + dx > canvas.width-ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if(y + dy > canvas.height-ballRadius || y + dy < ballRadius) {
        dy = -dy;
    }

    x += dx;
    y += dy;
}

setInterval(draw, 10);
