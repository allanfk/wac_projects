<?php

namespace Acme;

use Acme\Teams\Team;

class Person {
  private $name;
  private $teams = [];

  public function __construct($name) {
    $this->name = $name;
  }

  public function getName() {
    echo $this->name;
    // return $this->name;
  }

  public function favorite(Team $team) {
    $this->teams[] = $team->getName();
    $team->favorite();
    return $team->getName();
  }

  public function listFavorites(){
    $teamsNbr = count($this->teams);
    if ($teamsNbr == 1) {
      echo $this->teams[0];
    } else {
      for ($i=0; $i < $teamsNbr; $i++) { 
        echo $this->teams[$i];
        if ($i < $teamsNbr - 1){
          echo ' et de ';
        }
      }
    }
  }
}