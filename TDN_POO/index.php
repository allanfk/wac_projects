<?php
### TEST DE FAKER

echo '<pre>';

// require the Faker autoloader
require_once 'vendor/autoload.php';
// alternatively, use another PSR-0 compliant autoloader (like the Symfony2 ClassLoader for instance)

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

// generate data by accessing properties
echo $faker->name;
echo '<br>';
  // 'Lucy Cechtelar';
echo $faker->address;
echo '<br>';
  // "426 Jordy Lodge
  // Cartwrightshire, SC 88120-6700"
echo $faker->text;
echo '<br>';
  // Dolores sit sint laboriosam dolorem culpa et autem. Beatae nam sunt fugit
  // et sit et mollitia sed.
  // Fuga deserunt tempora facere magni omnis. Omnis quia temporibus laudantium
  // sit minima sint.
echo '</pre>';

### TEST DE LA POO

use Acme\{Person, League, Teams\Team};

echo random_number() . '<br>';

$honore = new Person("Honore Lancelot");
$pierre = new Person("Pierre Dubuc");

$barcelona = new Team("Barcelona");
$realMadrid = new Team("Real Madrid");
$psg = new Team("Paris-Saint-Germain");
$om = new Team("Olympique de Marseille");
$ol = new Team("Olympique Lyonnais");


$honore->favorite($barcelona);
$honore->favorite($realMadrid);
$honore->favorite($om);
$pierre->favorite($realMadrid);

echo $honore->getName() . ' est fan de ';
$honore->listFavorites();
echo "<br>";

echo $pierre->getName() . ' est fan de ';
$pierre->listFavorites();
echo "<br>";

// echo $barcelona->getName() . ' a ' . $barcelona->getNbrFans() . ' fans';
// echo '<br>';
// echo $realMadrid->getName() . ' a ' . $realMadrid->getNbrFans() . ' fans';
// echo '<br>';

$liga = new League("Liga");
$liga->addTeam($barcelona);
$liga->addTeam($realMadrid);

echo "Nombre d'équipe = " . $liga->getTeamCount($barcelona);