<?php

namespace Core;
use \PDO;

abstract class DataBase
{
	private static $_connection;
    private static $_database;
    private static $_user;
    private static $_password;
	private $_query;

	final protected function getInstance()
	{
		if(self::$_connection === NULL) {
			try {
			    $this->getConnection();

				self::$_connection = new PDO(self::$_database, "root", "");
			} catch(\Exception $e) {
				die("Error : ".$e->getMessage());
			}
		}
		return self::$_connection;
	}

	private function getConnection()
    {
        $config = file_get_contents("config/bdd.json");
        $config = json_decode($config, true);

        self::$_database = "mysql:host=" . $config["host"] . ";dbname=" . $config["dbname"] . ";charset=" . $config["charset"];
        self::$_user = $config["username"];
        self::$_password = $config["password"];
    }

	protected function query($query)
	{
        $this->getConnection();

        $this->_query = $this->getInstance()->prepare($query);
	}

	protected function bind($param, $value, $type = NULL)
	{
		$type = (is_int($value)) ? PDO::PARAM_INT : PDO::PARAM_STR;
		$this->_query->bindValue($param, $value, $type);
	}

	protected function execute()
	{
		return $this->_query->execute();
	}

	protected function results()
	{
		$this->execute();
		return $this->_query->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function result()
	{
		$this->execute();
		return $this->_query->fetch(PDO::FETCH_ASSOC);
	}

	protected function rowCount()
	{
		return $this->_query->rowCount();
	}

	protected function lastInsertId()
	{
		return $this->getInstance()->lastInsertId();
	}

	protected function beginTransaction()
	{
		return $this->getInstance()->beginTransaction();
	}

	protected function endTransaction()
	{
		return $this->getInstance()->commit();
	}

	protected function cancelTransaction()
	{
		return $this->getInstance()->rollBack();
	}

	private function __clone(){}
}