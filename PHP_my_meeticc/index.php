<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <meta charset="utf-8"/>
  <title>MyMeetic</title>
  <!-- CSS -->
  <link rel="stylesheet" href="assets/css/profile.css">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://hammerjs.github.io/dist/hammer.min.js"></script>
  <script src="assets/js/tinder.js"></script>
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- Font awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Knewave" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
  <link href="assets/fonts/Neon.ttf">
</head>

<body>
  <?php include 'db/c.php' ?>
  <?php include 'controller/selector.php' ?>
  <!-- BG -->
  <!--<img src="assets/img/vignette-focus.jpg" alt="" class="bg">-->
  <!-- NAV BAR -->
  <nav class="navbar navbar-inverse bg-white">
    <div class="container-fluid">
      <div class="navbar-header">
        <img src="assets/img/vectorpaint.svg" alt="" class="brand-icon">
        <a class="navbar-brand bg-black" href="#"><p>Start</p><p class="yellow">One</p></a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> Bonjour Luigi</a>
          <ul class="dropdown-menu">
            <li><a href="#">Messagerie</a></li>
            <li><a href="#">Matchs</a></li>
            <li>----------------------------------</li>
            <li><a href="#">Déconnexion</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>

  <div class="tinder">
  <div class="tinder--status">
    <i class="fa fa-remove"></i>
    <i class="fa fa-heart"></i>
  </div>

  <div class="tinder--cards">
    <?php
    foreach($profiles as $profile)
      {
        if ($profile['skill'] == 'java') {
          $iconSkill = 'assets/img/java.svg';
        }

        echo '<div class="tinder--card">';
        echo '<div class="container-profile">';
        echo '<img src=" ' . $profile['avatar'] . ' " class="profile">';
        echo '<img src="' . $iconSkill . '" class="skill">';
        echo '<p class="time">' . $profile['level'] . '</p>';
        echo '</div>';
        echo '<h3>' . $profile['prenom'] . ',' . $profile['ville'] . '</br>' . utf8_encode($profile['ecole']) . '</h3>';
        echo '<p>'. utf8_encode($profile['description']) . '</p>';
        echo '<a href="' . $profile['link_siteOne'] . '"><img src="' . $profile['prev_siteOne'] . '" class="portfolio"></a>';
        echo '</div>';
      }
    ?>

    <div class="tinder--card">
      <div class="container-profile">
      <img src="assets/img/profile/pp1.jpg" class="profile">
      <img src="assets/img/java.svg" class="skill">
      <p class="time">6m</p>
      </div>
      <h3>Moudine, PARIS</br>42</h3>
      <p>Étudiant en 4ème année à Epitech. Je pratique le développement web depui plus de 2 ans. Je suis expert en php avec les frameworks Symfony et Laravel.</p>
      <!-- <div class="gradient-title">-->
        <a href="https://www.w3schools.com"><img src="assets/img/porto1.png" class="portfolio"></a>
      <!--</div>-->
    </div>
    <div class="tinder--card">
      <div class="container-profile">
      <img src="assets/img/profile/25039173_136957420347698_3773080275060260864_n.jpg" class="profile">
      <img src="assets/img/icon/ruby.svg" class="skill">
      <p class="time">1y</p>
      </div>
      <h3>Marie, PARIS</br>Epitech</h3>
      <p>Étudiant en 4ème année à Epitech. Je pratique le développement web depui plus de 2 ans. Je suis expert en php avec les frameworks Symfony et Laravel.</p>
      <!-- <div class="gradient-title">-->
        <a href="https://www.w3schools.com"><img src="assets/img/test.png" class="portfolio"></a>
      <!--</div>-->
    </div>
    <div class="tinder--card">
      <div class="container-profile">
      <img src="assets/img/profile/19875661_10211509670911179_1388964044302440695_n.jpg" class="profile">
      <p class="skill p">C</p>
      <p class="time">2y</p>
      </div>
      <h3>Simon, LYON</br>Paris 8 Université</h3>
      <p>Étudiant en 4ème année à Epitech. Je pratique le développement web depui plus de 2 ans. Je suis expert en php avec les frameworks Symfony et Laravel.</p>
      <!-- <div class="gradient-title">-->
        <a href="https://www.w3schools.com"><img src="assets/img/porto1.png" class="portfolio"></a>
      <!--</div>-->
    </div>
    <div class="tinder--card">
      <div class="container-profile">
      <img src="assets/img/profile/26272635_204447823437854_7782780108162465792_n.jpg" class="profile">
      <p class="main-skill p">c</p>
      <img src="assets/img/42_Logo.svg" class="scholl">
      <p class="time">6m</p>
      </div>
      <h3>Moudine Mhoma</br>Lille</h3>
      <p>Étudiant en 4ème année à Epitech. Je pratique le développement web depui plus de 2 ans. Je suis expert en php avec les frameworks Symfony et Laravel.</p>
      <!-- <div class="gradient-title">-->
        <img src="assets/img/porto1.png" class="portfolio">
      <!--</div>-->
    </div>
    <div class="tinder--card">
      <img src="assets/img/profile/26272635_204447823437854_7782780108162465792_n.jpg" class="profile">
      <h3>Kalistouille la fripouille</h3>

      <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>
    </div>
  </div>

  <div class="tinder--buttons">
    <button id="nope"><i class="fa fa-remove"></i></button>
    <button id="love"><i class="fa fa-heart"></i></button>
  </div>
</div>

  <script src='https://hammerjs.github.io/dist/hammer.min.js'></script>
  <script  src="assets/js/tinder.js"></script>
</body>
</html>
