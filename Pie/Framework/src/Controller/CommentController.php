<?php

namespace Controller;


use Core\Controller;
use Model\CommentModel;

class CommentController extends Controller
{
    public function findAction($params = [])
    {
        $comment = new CommentModel(["id" => $params["id"]]);
        $comment->oneRelation("article");
        $comment->oneRelation("user");
        $comment->manyRelation("articles");
        var_dump($comment->getRelations());
        var_dump($comment->read());
    }
}