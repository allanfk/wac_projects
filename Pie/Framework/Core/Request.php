<?php

namespace Core;

class Request
{
    private $_sanitized = [];

    private function sanitize($array)
    {
        $sanitized = array_map(function($item) {
            return trim(addslashes(htmlspecialchars($item)));
        }, $array);

        return $sanitized;
    }

    public function secureHttp($http = [])
    {
        if(empty($http)) {
            $http = ["post" => $_POST, "get" => $_GET];
        }

        foreach($http as $key => $value) {
            $this->_sanitized[$key] = $this->sanitize($value);
        }

        return $this->_sanitized;
    }
}