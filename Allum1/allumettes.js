function square(args) {
    lines = args[0];
    space = (args[0])/2;
    allumette = "|";
    i=0;

    for (let index = -1; index < lines; index++) {
        process.stdout.write("*");
    }

    process.stdout.write("\n");

    for (let index = 0; index < args[1]; index++) {
        process.stdout.write("*");
        if (space > 0) {
            process.stdout.write(" ".repeat(space));
        }
        process.stdout.write(allumette);
        if (space > 0) {
            process.stdout.write(" ".repeat(space));
            allumette = allumette.concat("||");
        }
        process.stdout.write("*\n");
        space -= 1;
    }

    for (let index = 0; index < args[0]; index++) {
        process.stdout.write("*");
    }
}
square(process.argv.slice(2));