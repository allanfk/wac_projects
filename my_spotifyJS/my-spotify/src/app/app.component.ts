import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title:string = 'Liste des pokémons :';
  private pokemons: Pokemon[];
  private value:string = '';

  ngOnInit() {
    this.pokemons = POKEMONS;
  }

  onClick() {
    console.log("Clic !")
  }

  onKey2(event: KeyboardEvent) {
    this.value = 'Bonjour ' + (<HTMLInputElement>event.target).value;
  }

  onKey(value: string) {
    this.value = 'Bonjour ' + value;
  }

  selectPokemon(pokemon: Pokemon) {
    alert("Vous avez cliqué sur " + pokemon.name);
  }
}
