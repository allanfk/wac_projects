<?php

namespace Core;

class ORM extends DataBase
{

    public function create($table, $fields = [])
    {
        $placeHolder = $this->getPlaceHolders($fields);
        $query = "INSERT INTO $table(" . implode(", ", array_keys($fields)) . ") VALUES(" . implode(", ", $placeHolder) . ")";

        $this->query($query);
        $this->bindMultiples($fields);
        $this->execute();

        return $this->lastInsertId();
    }

    public function find($table, $with, $rel = [])
    {
        $query = "SELECT * FROM $table";
        $query .= $this->joinOneRelations($table, $rel);
        $query .= $this->joinSearchArgs($with);

        $this->query($query);
        $this->bindMultiples($with);

        return $this->result();
    }

    public function update($table, $id, $fields = [])
    {
        $placeHolder = $this->getPlaceHolders($fields);
        $links = $this->linkPlaceHolders(array_keys($fields), $placeHolder);
        $query = "UPDATE $table SET " . implode(", ", $links) . " WHERE $table.id = :id";

        $this->query($query);
        $this->bindMultiples($fields);
        $this->bind("id", $id);

        return $this->execute();

    }

    public function delete($table, $id)
    {
        $this->query("DELETE FROM $table WHERE $table.id = :id");
        $this->bind("id", $id);

        return $this->execute();
    }

    public function read($table, $params = ["WHERE" => "1", "ORDER BY" => "id ASC", "LIMIT" => ""], $rel = [])
    {
        $query = "SELECT * FROM $table";
        $query .= $this->bindSearchParams($params);

        $this->query($query);
        $results = $this->results();

        return $this->getRelatedRecords($table, $results, $rel);
    }


    private function joinSearchArgs($search)
    {
        $query = " WHERE";

        foreach($search as $key => $value) {
            $query .= " $key = :$key ";

            if(end($search) !== $value) {
                $query .= "AND";
            }
        }

        return $query;
    }

    private function getPlaceHolders($args)
    {
        $placeHolders = array_map(function($key) {
            return ":$key";
        }, array_keys($args));

        return $placeHolders;
    }

    private function linkPlaceHolders($keys, $values)
    {
        $links = array_map(function($key, $value){
            return "$key = $value";
        }, $keys, $values);

        return $links;
    }

    private function bindSearchParams($params)
    {
        $search = "";

        foreach($params as $key => $value) {
            if(empty($value)) {
                continue;
            }

            $search .= " $key ";

            if(is_array($value)) {
                $search .= implode(" ", $value);
            } else {
                $search .= $value;
            }
        }

        return $search;
    }

    private function bindMultiples($values)
    {
        foreach($values as $key => $value) {
            $this->bind($key, $value);
        }
    }

    private function joinOneRelations($table, $rel = [])
    {
        $query = "";

        if(!isset($rel["has one"])) {
            return $query;
        }

        foreach($rel["has one"] as $key => $value) {
            $table_name = "${value}s";

            $query .= " INNER JOIN $table_name ON $table_name.id = $table.${value}_id";
        }

        return $query;
    }

    private function getRelatedRecords($table, $results, $rel = [])
    {
        $query = [];

        for($i = 0; $i < count($results); $i++) {
            $value = $results[$i];

            $results[$i]["relations"] = array_map(function($relations) use ($table, $value) {
                $table_id = preg_replace("/s$/", "", $table);

                $this->query("SELECT * FROM $relations WHERE $relations.${table_id}_id = :id");
                $this->bind("id", $value["id"]);

                return [$relations => $this->results()];
            }, $rel["has many"]);
        }

        return $results;
    }
}