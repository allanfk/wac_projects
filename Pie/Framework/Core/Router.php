<?php

namespace Core;

class Router
{
    private static $routes;

    public static function connect ($url, $route, $param = [])
    {
        foreach($param as $key => $value) {
            $url = preg_replace("/:${key}/", "($value)", $url);
        }

        self::$routes[$url] = ["routes" => $route, "params" => $param];
    }

    public static function get ($url)
    {
        $routerResult = Router::getMatchingUrl($url);

        if(isset($routerResult["actionControl"])) {
            return $routerResult;
        }

        return ["actionControl" => ["controller" => "error", "action" => "error"], "params" => []];
    }

    static private function getMatchingUrl($url)
    {
        $matches = [];

        foreach(self::$routes as $key => $value) {

            if(preg_match("#^${key}$#", $url, $matches)) {
                unset($matches[0]);
                $matches = array_combine(array_keys($value["params"]), $matches);

                return ["actionControl" => $value["routes"], "params" => $matches];
            }
        }

        return false;
    }
}