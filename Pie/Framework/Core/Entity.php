<?php

namespace Core;

abstract class Entity
{
    private $_table;
    private static $_orm;
    private static $_relations = [
        "has one" => [],
        "has many" => [],
    ];

    public function __construct($params = [])
    {
        if(is_null(self::$_orm)) {
            self::$_orm = new ORM();
        }

        $this->getTable();
        $params = $this->checkParams($params);

        $this->getParams($params);
    }

    public function save()
    {
        $objectProperties = get_object_vars($this);
        $classProperties = get_class_vars(get_class());
        $params = array_diff_key($objectProperties, $classProperties);

        return $this->getORM()->create($this->_table, $params);
    }

    public function read()
    {
        return $this->getORM()->read($this->_table, [], self::$_relations);
    }

    public function find()
    {
        $objectProperties = get_object_vars($this);
        $classProperties = get_class_vars(get_class());
        $params = array_diff_key($objectProperties, $classProperties);

        return $this->getORM()->find($this->_table, $params, self::$_relations);
    }

    public function update()
    {
        $objectProperties = get_object_vars($this);
        $classProperties = get_class_vars(get_class());
        $params = array_diff_key($objectProperties, $classProperties);

        return $this->getORM()->update($this->_table, $this->id, $params);
    }

    public function delete()
    {
        $this->getORM()->delete($this->_table, $this->id);
    }

    private function getORM()
    {
        return self::$_orm;
    }

    private function getTable()
    {
        $class = basename(get_called_class());
        $this->_table = strtolower(preg_replace("/Model/", "", $class)) . "s";
    }

    private function getParams($params)
    {
        foreach($params as $key => $value) {
            $this->{$key} = $value;
        }
    }

    private function checkParams($params)
    {
        if(isset($params["id"]) && $params["id"]) {
            $realParams = $this->getORM()->find($this->_table, $params);

            if($realParams) {
                return $realParams;
            }
        }

        return $params;
    }

    public function oneRelation($table)
    {
        self::$_relations["has one"][] =  $table;
    }

    public function manyRelation($table)
    {
        self::$_relations["has many"][] = $table;
    }

    public function getRelations()
    {
        return self::$_relations;
    }
}