<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cuisine ta race</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>

  <body>
    <? include 'model.php'; ?>
    <!-- search bar -->
    <div class="wrap">
       <div class="search">
         <form action="index.php" method="post">
          <input type="text" class="searchTerm" name='search' placeholder="Tu cherche quoi fdp ?">
          <button type="submit" class="searchButton">
            <i class="fa fa-search"></i>
         </button>
        </form>
       </div>
    </div>
    <div class="container list">
      <div class="row">
        <div class="col-xs-12"></div>
          <table class="table table-dark">
            <thead>
              <tr>
                <th scope="col">Nom</th>
                <th scope="col">Photo</th>
                <th scope="col">Instruction</th>
                <th scope="col">Pour</th>
                <th scope="col">Ingredients</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?
                if ($n == true) {
                  echo '<td>' . $parsed_json[$n]['title'] . '</th>';
                  echo '<td><img src="img/' .$parsed_json[$n]['image_name'] . '" class="cover_food"></th>';
                  echo '<td>' . $parsed_json[$n]['instructions'] . '</th>';
                  echo '<td>' . $parsed_json[$n]['servings'] . ' personnes </th>';
                  /*
                  echo 'NOM : ' . $parsed_json[$n]['title'] . '<br>';
                  echo 'INSTRUCTIONS : ' . $parsed_json[$n]['instructions'] . '<br>';*/
                }
                ?>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
    <img src="https://img00.deviantart.net/4b32/i/2013/005/f/8/cook_kirby_by_krukmeister-d5qkhmy.png" class="brand-icon">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
