<?php

namespace Acme\Teams;

class Team {
  private $name;
  private $nbrFans = 0;

  public function __construct($name){
    $this->name = $name;
  }

  public function getName(){
    return $this->name;
  }

  public function getNbrFans(){
    return $this->nbrFans;
  }

  public function favorite() {
    $this->nbrFans++;
  }
}
