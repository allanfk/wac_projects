<?php
$nbr_total_articles = $to_pag->rowCount();
$nbr_articles_page = 10;
$nbr_articles_max_last_past = 5;
$last_page = ceil($nbr_total_articles / $nbr_articles_page);

if (isset($_GET['page']) && is_numeric($_GET['page'])){
  $page_num = $_GET['page'];
} else {
  $page_num = 1;
}

if($page_num < 1){
  $page_num = 1;
} else if ($page_num > $last_page) {
  $page_num = $last_page;
}

$limit = ($page_num-1) * $nbr_articles_page;

$query .= "LIMIT  $nbr_articles_page
           OFFSET $limit";
$to_pag = $db->query($query);

$pagination = '';

if($last_page != 1) {
  if($page_num > 1) {
    $previous = $page_num -1;
    $pagination .= '<a href="?mot=' . $_GET['mot'] . '&&page=' . $previous . '">Précédent</a> &nbsp; &nbsp;';
    for($i = $page_num - $nbr_articles_max_last_past; $i < $page_num; $i++){
      if($i > 0) {
        $pagination .= '<a href="?mot=' . $_GET['mot'] . '&&page=' . $i . '">' . $i . '</a> &nbsp;';
      }
    }
  }

  $pagination .= '<span class="active">' . $page_num . '</span>&nbsp;';

  for ($i = $page_num+1; $i <= $last_page; $i++){
    $pagination .= '<a href="?mot=' . $_GET['mot'] . '&&page=' . $i .'">' . $i . '</a>';
    if($i >= $page_num + $nbr_articles_max_last_past){
      break;
    }
  }

  if($page_num != $last_page){
    $next = $page_num + 1;
    $pagination .= '<a href="?mot=' . $_GET['mot'] . '&&page=' .  $next . '">Suivant</a> ';
  }
}

function button(){
  GLOBAL $nbr_total_articles, $page_num, $last_page, $pagination;
  echo "<p><strong>($nbr_total_articles)</strong> articles au total ! <br/>";
  echo "Page <b>$page_num</b>sur<b>$last_page</b></p>";
  echo '<div id="pagination">' . $pagination . '</div>';
}
?>
