<?php

namespace Core;

class TemplateEngine
{
    private $_template;
    private $_output;
    private $_regexp = [
        "/{{(.+)}}/" => "<?= htmlspecialchars ($1) ?>",
        "/@(if|elseif|switch|foreach|case)(.+)\N?/" => "<?php $1$2 : ?>",
        "/@end(if|elseif|switch|foreach)\N?/" => "<?php end$1 ; ?>",
        "/@(\w+\s?)\((.+)\)\N?/" => "<?php if($1($2)) : ?>",
        "/@(.+)\N/" => "<?php $1; ?>",
        "/(\r)/" => "",
    ];

    public function __construct($template)
    {
        if(!file_exists($template)) {
            return false;
        }

        $this->_template = file_get_contents($template);
        $this->replace();

        return true;
    }

    private function replace()
    {
        $template = $this->_template;

        foreach($this->_regexp as $regexp => $replacement) {
            $template = preg_replace($regexp, $replacement, $template);
        }

        $this->_output = $template;
    }

    public function parse()
    {
        file_put_contents("src/View/Template/template.php", $this->_output);

        return true;
    }
}
