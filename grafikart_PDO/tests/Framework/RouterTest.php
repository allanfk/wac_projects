<?php

namespace Test\Framework;

use Framework\App;
use Framework\Router;
//use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\ServerRequest;
//use Framework\Router\MiddlewareApp;

class RouterTest extends TestCase {
  
  /**
   * @var [Router]
   */

  private $router;

  public function setUp()
  {
    $this->router = new Router();
  }

  /*
   * 1. Creation d'une fausse requete GET->"/blog".
   * 2. Au niveau du router : création d'une nouvelle url "/blog" qui retourne "Hello"
   *    et d'on le nom est 'blog'.
   * 3. Uilisation de la methode 'match()' afin de savoir si l'url match avec une des url
   *    qui a était entrer. Si c'est le cas la méthode retournera l'objet '$route'.
   * 4. Utilisation de la méthode 'getName()' afin de récupérer le nom de la route 
   *    qui a matcher.
   * 5. Utilisation de la méthode 'getCallback()' afin de récupérer la méthode utiliser.
   */

  public function testGetMethod()
    {
        $request = new ServerRequest('GET', '/blog');
        $this->router->get('/blog', function () { return 'Hello'; }, 'blog');
        $route = $this->router->match($request);
        $this->assertEquals('blog', $route->getName());
        $this->assertContains('Hello', call_user_func_array($route->getCallback(),[$request]));
    }
  
  // Test sur un lien qui n'existe pas.

  public function testGetMethodIfURLDoesNotExists() {
    $request = new ServerRequest('GET', "/blog");
    $this->router->get("/blogaze", function () { return 'hello'; }, 'blog');
    $route = $this->router->match($request);
    $this->assertEquals(null, $route);
  }

  public function testGetMethodWithParameters() {
    $request = new ServerRequest('GET', "/blog/mon-slug-8");
    $this->router->get("/blog", function () { return 'azeaze'; }, 'posts');
    $this->router->get('/blog/{slug:[a-z0-9\-]+}-{id:\d+}', function () { return 'hello'; }, 'post.show');
    $route = $this->router->match($request);
    $this->assertEquals('post.show', $route->getName());
    $this->assertEquals('hello', call_user_func_array($route->getCallback(), [$request]));
    $this->assertEquals(['slug' => 'mon-slug', 'id' => '8'], $route->getParams());
    // Test invalid url
    $route = $this->router->match(new ServerRequest('GET', '/blog/mon_slug-8'));
    $this->assertEquals(null, $route);
  }
  
  public function testGenerateUri() {
    // $request = new ServerRequest('GET', "/blog/mon-slug-8");
    $this->router->get("/blog", function () { return 'azeaze'; }, 'posts');
    $this->router->get('/blog/{slug:[a-z0-9\-]+}-{id:\d+}', function () { return 'hello'; }, 'post.show');
    $uri = $this->router->generateUri('post.show', ['slug' => 'mon-article', 'id' => 18]);
    $this->assertEquals('/blog/mon-article-18', $uri);
    //$this->assertEquals('hello', call_user_func_array($route->getCallback(), [$request]));
    //$this->assertEquals(['slug' => 'mon-slug', 'id' => '8'], $route->getParams());
  }
}