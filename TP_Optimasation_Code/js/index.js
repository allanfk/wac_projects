
(function () {

    const DataTable = module.import('DataTable');
    const TextCmp = module.import('TextCmp');

    window.addEventListener('DOMContentLoaded', function () {

        let dt = new DataTable({ dataSet: [] }, '#list')
        let numberFrom = new TextCmp({ value: null }, '#people-from-count')
        let numberDiet = new TextCmp({ value: null }, '#people-from-count')

        document.querySelector('#people-fieldset button').addEventListener('click', function() {
            fetch('api.php?resource=people').then(function (data) {
                return data.ok && data.json();
            }).then(function (dataArray) {
                let dataSet = dataArray
                dt.setProps({ dataSet })
            });    
        })
        
        document.querySelector('#people-fieldset input[name=mail_domain]').addEventListener('keyup', function() {
            let val = this.value;

            fetch('api.php?resource=countPeopleFrom&mail_domain=' + val).then(function (data) {
                return data.ok && data.json();
            }).then(function (data) {
                numberFrom.setProps({ value: data.count })
            });
        })


        document.querySelector('#people-fieldset #checkbox-wrapper').addEventListener('change', function() {
            let values = []
            this.querySelectorAll('input').forEach(function (chk) {
                chk.checked && values.push(chk.value)
            });

            fetch('api.php?resource=countPeopleByDiet&str=' + values.join(',')).then(function (data) {
                return data.ok && data.json();
            }).then(function (data) {
                numberDiet.setProps({ value: data.count })
            });


        })

    })

})()
