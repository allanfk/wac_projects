<?php

namespace Framework;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App
{
  /**
   * Undocumented variable
   *
   * @var array
   */
  private $modules = [];
  /**
   * Undocumented variable
   *
   * @var Router
   */
  private $router;

  /**
   * App constructor
   * param string[] $modules liste des modules à  
   */

  public function __construct(array $modules = [])
  {
    $this->router = new Router();
    foreach($modules as $module) {
      $this->modules[] = new $module($this->router);
    }
  }

  public function run(ServerRequestInterface $request): ResponseInterface
  {
    $uri = $request->getUri()->getPath();

    if (!empty($uri) && substr($uri, -1, 1) === "/") {
      return (new Response())
        ->withStatus(301)
        ->withHeader("Location", substr($uri, 0, -1));
    }

    $route = $this->router->match($request);

    if(is_null($route)) {
      return new Response(404, [], '<h1>Erreur 404</h1>');
    }

    $params = $route->getParams();
        $request = array_reduce(array_keys($params), function ($request, $key) use ($params) {
            return $request->withAttribute($key, $params[$key]);
        }, $request);

    //$request->withAttribute();

    $reponse = call_user_func_array($route->getCallback(), [$request]);
    
    if (is_string($reponse)) {
        return new Response(200, [], $reponse);
    } elseif ($reponse instanceof ResponseInterface) {
      return $response;
    } else {
      throw new \Exception('The response is not a string or an instance of ResponseInterface');
    }
    
    if ($uri === '/blog/mon-article') {
      return new Response(200, [], "<h1>Bienvenue sur l'article de test</h1>");
    }
    
    if ($uri === '/blog') {
      return new Response(200, [], "<h1>Bienvenue sur le blog</h1>");
    }
    
    return new Response(404, [], "<h1>Erreur 404</h1>");
  }
}
