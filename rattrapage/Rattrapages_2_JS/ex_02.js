function array_sort(arr) {
  var reverse = [];
  for (var i=0; i<arr.length; i++) {
    var check = i+1;
    if (check < arr.length) {
      check = i+1;
    } else {
      check = i;
    }
    if(typeof arr[i] == 'string') {
      if (arr[i].charCodeAt(0) > arr[check].charCodeAt(0)) {
        reverse.unshift(arr[i]);
      } else {
        reverse.push(arr[i])
      }
    } else if (typeof arr[i] == 'number') {
      index = arr[i];
      j = i;
      while (j > 0 && arr[j-1] < index) {
        arr[j] = arr[j-1];
        j--;
      }
      arr[j] = index;
      return arr[j];
    }
  }
  return reverse;
}

var tab = ['baz', 'foo', 'bar'];
array_sort(tab); // ['foo', 'baz', 'bar']
console.log(tab); // ['baz', 'foo', 'bar']

tab = [10, 54, 268, 25];
array_sort(tab); // [268, 54, 25, 10]
console.log(tab); // [10, 54, 268, 25]
