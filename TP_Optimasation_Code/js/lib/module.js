var module = [];

module.__proto__.import = function (name) {
    if (module[name] !== undefined) {
        return module[name]
    } else {
        throw Error(`Module ${name} not exists - check script tags in html`)
    }
}

module.__proto__.exports = function (name, value) {
    module[name] = value
}

