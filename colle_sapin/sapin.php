<?php
$size = $argv[1];
function sapin(int $size) {
  if ($size == 0 || empty($size))   {
    echo '';
  }
  $star = '*';
  $box=8;
  $space = 8 * $size/2;
  $space2 = 8 * $size/2;
  $restart = 1;
  for ($i=0; $i<$size; $i++) {
    for($j=$restart; $j<$box; $j++) {
      if ($j%2!=0) {
        //echo $space;
      echo str_repeat(' ', $space);
      $row = str_repeat($star, $j) . "\n";
      echo $row;
      $space -= 1;
      }
      if ($j > 20 && $j == $box - 2) {
        $restart = $j-4;
        $space += 3;
      }
      else if ($j == $box-1 && $j<19) {
        $restart=$j-2;
        $space=$space+2;
      }
    }
    $box= $box * 1.5;
  }

  for ($i=1; $i<=$size; $i++) {
    echo str_repeat(' ', $space2-$size+1);
    echo str_repeat('| ', $size) . "\n";
  }
}

sapin($size);
?>
