<?php

namespace Core;


class Session
{
    static function start()
    {
        if(!isset($_SESSION)) {
            session_start();
        }
    }

    static function unsetSession()
    {
        if(isset($_SESSION)) {
            session_unset();
        }
    }

    static function fill($id)
    {
        $_SESSION["id"] = $id;
    }

    static function destroy()
    {
        session_destroy();
    }

    static function get()
    {
        if(!isset($_SESSION["id"])) {
            return null;
        }

        return $_SESSION["id"];
    }
}