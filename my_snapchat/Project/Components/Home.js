import React from 'react';
import { TouchableOpacity, Alert, StyleSheet, Text, View, TextInput, Image, Button } from 'react-native';

class Home extends React.Component {
    render() {
      _displaySignUp = () => {
        this.props.navigation.navigate("SignUp")
      }
      return (
        <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image source={require('../assets/snap_icon.png')} style={styles.image}/>
        </View>
        <View style={[styles.elementsContainer]}>
          <TouchableOpacity
            style={[{backgroundColor: '#E92754'}, styles.button]}
            onPress={() => {Alert.alert('You tapped the button!');}}>
              <Text style={styles.textButton}> CONNEXION </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[{backgroundColor: '#3CB2E2'}, styles.button]}
            onPress={() => _displaySignUp()}>
              <Text style={styles.textButton}> INSCRIPTION </Text>
          </TouchableOpacity>
        </View>
      </View>
      )
    }
}
export default Home

const styles = StyleSheet.create({
  container: {
    paddingTop: 48,
    flex: 1,
    backgroundColor: '#F3EF1A'
  },
  headerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 70,
    height: 70
  },
  headerStyle: {
    fontSize: 36,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: '100',
    marginBottom: 24
  },
  elementsContainer: {
    justifyContent: 'flex-end',
    flex: 1,
  },
  button: {
    height: 90,
    justifyContent: 'center',
  },
  textButton: {
    color : 'white',
    fontSize: 40,
    textAlign: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
  }
})