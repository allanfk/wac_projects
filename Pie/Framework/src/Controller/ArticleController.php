<?php

namespace Controller;


use Core\Controller;
use Model\ArticleModel;

class ArticleController extends Controller
{
    public function findAction($params = [])
    {
        $article = new ArticleModel();
        $article->manyRelation("comments");
    }
}