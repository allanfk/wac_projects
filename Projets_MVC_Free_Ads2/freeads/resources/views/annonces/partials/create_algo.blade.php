<div class="container">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" method="post" action="{{ action('AnnonceController@store') }}" enctype="multipart/form-data">
          @csrf
          <fieldset>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Nom</label>
              <div class="col-md-9">
                <input id="name" name="name" type="text" placeholder="Nom" class="form-control">
              </div>
            </div>

            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="prenom">Prix</label>
              <div class="col-md-9">
                <input id="prenom" name="price" type="number" placeholder="Prenom" class="form-control">
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Description</label>
              <div class="col-md-9">
                <input id="email" name="description" type="text" placeholder="Email" class="form-control">
              </div>
            </div>

            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="password">Categories</label>
              <div class="col-md-9">          
                {!! Form::select('category', $categories, null, ['class' => 'form-control']) !!}
              </div>
            </div>

            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="password">Photo</label>
              <div class="col-md-9">          
                  {!! Form::file('image'); !!}
              </div>
            </div>

            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-lg">Enregistrer</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>