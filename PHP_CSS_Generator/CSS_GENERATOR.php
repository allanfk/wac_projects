<?php
$tab = $argv; // Mettre les arguments dans un tableau
$par = $argc; // Compteur d'arguments
$allImgPath = my_recursive('.',array('png'));

function my_recursive($folder,$ext=array('png'),$recursif=false) {
	$files = array();
	$dir=opendir($folder);
	while ($file = readdir($dir)) {
		if ($file == '.' || $file == '..') continue;
		if (is_dir($folder.'/'.$file)) {
			if ($recursif==true)
				$files=array_merge($files, my_recursive($folder.'/'.$file, $ext));
		} else {
			foreach ($ext as $v) {
				if (strtolower($v)==strtolower(substr($file,-strlen($v)))) {
					$files[] = $folder.'/'.$file;
					break;
				}
			}
		}
	}
	closedir($dir);
	return $files;
}

function my_merge_image($allImgPath, $tab)
{
  $wallAjustX = []; // Background size
  $wallAjustY = [];
  foreach ($allImgPath as $key => $value) {
    list($width, $height) = getimagesize($value);
    if ($width == $height){
      $height = 100;
      $width = 100;
    }
    array_push($wallAjustX, $height);
    array_push($wallAjustY, $width);
  }
  $x = array_sum($wallAjustX);
  $y = max($wallAjustY);
  array_multisort($wallAjustY, SORT_DESC, $allImgPath); // Sort by size
	$background = imagecreatetruecolor($y, $x); // H / W
  $red = imagecolorallocate($background, 255, 0, 0);
  imagefill($background, 0, 0, imagecolortransparent($background, $red));
  $outputImage = $background;
  $i = 0;
  $w = 0;
	foreach ($allImgPath as $key => $value) // Resize images + Merge * with Background
	{
		$imgCreate = imagecreatefrompng($value);
    list($width, $height) = getimagesize($value);
    $sizeBackground = imagecreatetruecolor($width, $height); //Background
    imagefill($sizeBackground, 0, 0, imagecolortransparent($sizeBackground, $red));
    if ($width == $height) { // Resizing
      imagecopyresampled($sizeBackground, $imgCreate, 0, 0,	0, 0,	100, 100,	$width, $height);
    }
    elseif ($width != $height) {
      imagecopyresampled($sizeBackground, $imgCreate, 0, 0, 0, 0, $width, $height, $width, $height);
    }
    if ($width == $height) {$w=100;} // Placement
    else {$w=$width;}
    imagecopymerge($outputImage, $sizeBackground,0,$i,0,0, $w, $height, 100);
    if ($width == $height) {$i+=100;}
    else {$i+=$height;}
  }
	imagepng($outputImage, 'sprites.png');
}

function resizing($first_img_path)
{
	// Get images

		$imgCreate = imagecreatefrompng($first_img_path);
		$get_size = getimagesize($first_img_path);
		// Resizing
		$sizeBackground = imagecreatetruecolor(100, 100);
		imagecopyresampled($sizeBackground, $imgCreate,
		                   	0, 0, // coordonnées X, Y du point de destination.
		                   	0, 0, // coordonnées X, Y du point source.
		                   	100, 100,
		                   	$get_size[0], $get_size[1]); // 0 = W, 1 = H
		//imagecopymerge($outputImage, $imgCreate,0,$i,0,0, $x, $y,100);
		// $i=$i+$y;
	imagepng($sizeBackground, 'img/test2.png');
	// Images merge
	//imagecopymerge($outputImage, $img1,0,0,0,0, $x, $y,100);
	//imagecopymerge($outputImage, $img2,0,$y,0,0, $x, $y,100);
	//imagepng($outputImage, 'img/test.png');
}

function help()
{
	echo shell_exec('man ./man');
}
// Liste de toute les commandes
if($par >= 2 )
{
	switch($tab[1])
	{
		case "man":help();
			break;
    case "-i" || "-output-image=" && $argv:
      if(is_dir($tab[2])) my_merge_image($allImgPath, $tab);
      else echo "Veuillez saissir une adresse valide !\n";
      break;
    case "-r" || "-recursive" && $argv:$folder=$argv; var_dump(my_recursive('.',array('png')));
  		break;
		case "size":resizing('img/html5.png');
			break;
		default: echo "Mauvaise commande\n";
		break;
	}
}
?>
