<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script type="text/javascript">
	 $(function() {
        toastr.options.positionClass = "toast-bottom-right";
		toastr.error('<?= $message ?>');
	});
</script>
<?php echo $this->Html->script(['bootstrap.min', 'toastr.min']); ?>
<div class="message error" onclick="this.classList.add('hidden');"></div>
