<?php

namespace Core;

class Core
{
    private $_url;
    private $_routes;
    private $_controller;
    private $_action;
    private $_params;

    public function __construct()
    {
        $this->getUrl();

        if(!$this->_url) {
            $this->_url = "/";
        }

        $this->_routes = Router::get($this->_url);
        $actionControl = $this->_routes["actionControl"];

        $this->setController($actionControl["controller"]);
        $this->setAction($actionControl["action"]);
        $this->setParams($this->_routes);
    }

    private function getUrl()
    {
        $param = preg_replace("#" . BASE_URI . "\/(.+)$#", "$1", $_SERVER["REQUEST_URI"]);

        if(BASE_URI . "/" !== $param) {
            $this->_url = "/" . $param;
        }
    }

    private function setParams($params)
    {
        if(isset($params["params"])) {
            $this->_params = $params["params"];
        }
    }

    private function setController($controller)
    {
        $this->_controller = "\Controller\\" . ucfirst($controller) . "Controller";
    }

    private function setAction($action)
    {
        $this->_action = $action . "Action";
    }

    public function run()
    {
        $controller = new $this->_controller();
        $controller->{$this->_action}($this->_params);
    }
}